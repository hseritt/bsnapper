#!/usr/bin/env bash

# Configuration file for Bsnapper
# Most changes should only be done in the 'User configuration' section

# User configuration

# Directory where set images are permanently stored
BACKUP_DIR="$HOME/bsnapper-backups" 

# General Bsnapper configs - change only if needed 
# Set this to wherever you install Bsnapper - use a full path, 
# not a relative one or a wildcard
BASE_PATH="$HOME/Devel/bash_projects/Bsnapper"
TIMESTAMP_FORMAT="%d%h%Y-%H%M%S"

# Paths for commands - change only if needed

# mkdir
MKDIR="/bin/mkdir"

# cp
CP="/bin/cp"

# gzip
GZIP="/bin/gzip"

# bzip2
BZIP2="/bin/bzip2"

# rm
RMRF="/bin/rm -rf"

# tar
TAR="/bin/tar"

# mv
MV="/bin/mv"

# mysqldump
MYSQLDUMP="/usr/bin/mysqldump"
