#!/usr/bin/env bash

# Change nothing here unless you know what you are doing!

# Importing necessary configs and libs
. bsenv.sh

# Import all in lib directory
for f in $(ls lib)
do
	. lib/$f
done

show_banner

# Check to ensure backup directory exists.
if [ ! -d $BACKUP_DIR ]
then
	echo "Warning: Backup directory does not exist."
	echo "Creating Backup Directory as defined: ${BACKUP_DIR}"
	$MKDIR $BACKUP_DIR
else
	echo "Backup directory set as ${BACKUP_DIR}."
fi

# Check to ensure sets directory exists with set config files.
if [ ! -d "sets" ]
then
	echo "Warning: missing sets directory."
	echo "Fixing: creating directory called sets."
	$MKDIR sets
fi

# Check to ensure there are set definition files.
if [ "$(ls sets|egrep '.set$'|wc -l)" -lt 1 ]
then
	echo "Error: missing sets config files."
	echo "Fix: create file called 'main.set' and restart."
	exit 1
fi

# Backup routine
for f in $(ls sets)
do
	echo "Preparing backups from set configuration file: $f"
	. sets/$f

	# Setting timestamp
	echo "Creating tmp backup directory."
	ts=$(date +"$TIMESTAMP_FORMAT")

	# Creating tmp directory and setting backup set directory
	BACKUPARCH="$name-$ts"
	TMPBACKUPDIR="tmp/$BACKUPARCH"
	$MKDIR -p $TMPBACKUPDIR

	# Copying over artifacts to tmp backup directory
	echo "Backing up following artifacts as part of set $name."
	for artifact in "${artifacts[@]}"
	do
		echo $artifact
		$CP -rf $artifact $TMPBACKUPDIR
	done

	# Setting and creating database backup directory
	if [ "${#mysql_dbs[@]}" -gt "0" ]
	then
		db_backup_dir="$TMPBACKUPDIR/databases"
		$MKDIR $db_backup_dir

		# Back up MySQL databases
		for db in "${mysql_dbs[@]}"
		do
			dbuser=$(echo $db|cut -d':' -f1)
			dbpasswd=$(echo $db|cut -d':' -f2)
			dbhost=$(echo $db|cut -d':' -f3)
			dbport=$(echo $db|cut -d':' -f4)
			dbname=$(echo $db|cut -d':' -f5)
			echo "Backing up MySQL database: $dbname ."
			$MYSQLDUMP -u"$dbuser" -p"$dbpasswd" -h $dbhost -P $dbport $dbname > $db_backup_dir/$dbname.$ts.sql
		done
	fi

	# Creating proper backup directory for set if it doesn't exist
	echo "Creating backup directory for $name."
	project_backup_dir="$BACKUP_DIR/$name"
	$MKDIR -p $project_backup_dir

	# Transferring set directory to final backup dir
	echo "Moving artifacts to $name backup directory."
	$MV -f $TMPBACKUPDIR $project_backup_dir

	# Archiving set directory and removing it after a tar is created
	echo "Archiving $BACKUPARCH."
	cd $project_backup_dir
	$TAR cvf $BACKUPARCH.tar $BACKUPARCH # archive backup directory
	$RMRF $BACKUPARCH # clean directory

	# Compressing tar archive file
	if [ "$compress" == "true" ]
	then
		echo "Compressing archive files."
		case $compress_method in
			gzip)
				COMPRESS=$GZIP
				;;
			bzip2)
				COMPRESS=$BZIP2
				;;
			*)
				echo "Warning: $compress_method not supported. Not compressing."
				;;
		esac
		$COMPRESS $BACKUPARCH.tar
	else
		echo "Compressing not requested."
	fi

	cd $BASE_PATH

	echo "Cleaning up."
	$RMRF tmp

	echo
done

echo "Bsnapper backups complete."
exit 0




